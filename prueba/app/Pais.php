<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Clientes;


class Pais extends Model
{

    //En el modelo se hace referecia a las relaciones y artributos de la tabla en la base de datos
    
    protected $table = 'paises';

    protected $primaryKey = "idPaises";

    protected $fillable = ['nombrePais'];

    public function cliente()
    {

        return $this->hasMany(Clientes::class);
    }
}
