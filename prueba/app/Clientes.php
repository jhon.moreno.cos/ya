<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pais;
use App\User;
use App\Sexos;


class Clientes extends Model
{
    //En el modelo se hace referecia a las relaciones y artributos de la tabla en la base de datos

    protected $table = "clientes";

    protected $primaryKey = "idCliente";


    protected $fillable = ['nombreCliente', 'apellidoCliente', 'telefonoCliente', 'id_Paises', 'id_Departamento', 'id_Sexo'];

    public function paises()
    {
        return $this->belongsTo(Pais::class, 'id_Paises');
    }
  
    public function departaments()
    {
        return $this->belongsTo(departamentos::class, 'id_Departamento');
    }
    public function sexos(){
        return $this->belongsTo(Sexos::class,'id_Sexo');
    }
}
