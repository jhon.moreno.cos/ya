<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    //Validaciones de letras, mumeros, caracteres especiales, cantidad de caracteres, etc.
    public function rules()
    {

        return [
            'nombreCliente' => 'required|regex:/^[\pL\s\-]+$/u|max:30',
            'apellidoCliente' => 'required|regex:/^[\pL\s\-]+$/u|max:30',
            'telefonoCliente' => 'required|regex:/^[0-9]+$/|min:7|max:10',
            'id_Paises' => 'required|exists:paises,idPaises',
            'id_Departamento' => 'required|exists:departamentos,id_Departamento',
            'id_Sexo' => 'required|exists:sexos,idSexo',
        ];
    }

    //Atributos para los mensajes, cuando se requiere el nombre del input este sera el que mostrará
    public function attributes()
    {
        return [
            'nombreCliente' => 'Nombre del cliente',
            'apellidoCliente' => 'Apellido del cliente',
            'telefonoCliente' => 'Teléfono del cliente',
            'id_Paises' => 'Pais donde reside',
            'id_Departamento' => 'Departamento donde reside',
            'id_Sexo' => 'Sexo',
        ];
    }
}
