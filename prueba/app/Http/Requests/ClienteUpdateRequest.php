<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    //Validaciones de letras, mumeros, caracteres especiales, cantidad de caracteres, etc.

    public function rules()
    {
        return [
            'nombreCliente' => 'required|alpha|max:30',
            'apellidoCliente' => 'required|alpha|max:30',
            'telefonoCliente' => 'required|regex:/^[0-9]+$/|min:7|max:10',
            'id_Paises' => 'required|exists:paises,idPaises',
            'id_Departamento' => 'required|exists:departamentos,id_Departamento',
            'id_Sexo' => 'required|exists:sexos,idSexo',
        ];
    }

    //Atributos para los mensajes, cuando se requiere el nombre del input este sera el que mostrará
    public function attributes()
    {
        return [
            'nombreCliente' => 'Nombre del cliente',
            'apellidoCliente' => 'Apellido del cliente',
            'telefonoCliente' => 'Teléfono del cliente',
  
            'id_Paises' => 'Pais donde reside',
            'id_Departamento' => 'Departamento donde reside',
            'id_Sexo' => 'Sexo',
        ];
    }
}
