<?php

namespace App\Http\Controllers;


use App\Clientes;
use App\departamentos;
use App\Pais;

use App\Sexos;


use App\Http\Requests\ClienteStoreRequest;
use App\Http\Requests\ClienteUpdateRequest;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ClientesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //En este metodo muetra lo que requiere la vista de la tabla de clientes haciendo llamado a las relaciones de los modelos y la base de datos.
    public function index()
    {
        $clientes = Clientes::orderBy('idCliente')->get();
        $clientes->each(function ($clientes) {
            $clientes->paises;
            $clientes->departaments;

            $clientes->sexos;
        });
        return view('home', compact('clientes'));   
    }


    public function create()
    {
        return view('Cliente.prueba');

        //   return view('Cliente.create', compact('paises'));
        //  return view('Cliente.create', compact('users'));

    }


    public function show(Clientes $clientes)
    {
        //
    }


    //Este es el metodo que me almacena los datos ingresados en la base datos, tambien me llama la alerta cuando se ha creado el nuevo cliente.

    public function store(ClienteStoreRequest $request)
    {

        if ($request == null) {

            alert()->error('Title', 'Lorem Lorem Lorem');
        } else {
            Clientes::create($request->all());
            return  redirect()->route('inicio')->with('success', ' El cliente fue registrado');
        }
    }

    //Este metodo me llama los datos que son requeridos para modificar un registro, haciendo el llamado a cada una de los modelos por el id de cada una de las tablas, retornandolos a la vista de editar
    public function edit($id)
    {

        $sexss = Sexos::all();

        $paisess = Pais::all();
        $clientes = Clientes::find($id);

        $idPaises = $clientes->id_Paises;
        $departamentoos = departamentos::where('id_Paises',  $idPaises)->get();

        $nombrePais = Pais::find($idPaises);

        $id_Departamento = $clientes->id_Departamento;
        $nombreDepartamento = departamentos::find($id_Departamento);

        $idSexo = $clientes->id_Sexo;
        $tiposSexo = Sexos::find($idSexo);




        return view('edit')
            ->with('clientes', $clientes)
            ->with('paisess', $paisess)
            ->with('nombrePais', $nombrePais)

            ->with('departamentoos', $departamentoos)
            ->with('nombreDepartamento', $nombreDepartamento)
            ->with('sexss', $sexss)
            ->with('tiposSexo', $tiposSexo)
            ->with('idPaises', $idPaises)
            ->with('idSexo', $idSexo)
            ->with('idDepartamento', $id_Departamento);
    }
    //Este ya es el metodo que hace la actualizacion de los datos en la base de datos, tambien mostrando un mensaje de que se han actualizado los datos

    public function update(ClienteUpdateRequest $request, $id)
    {
        $datosCliente = request()->except(['_token', '_method']);


        Clientes::where('idCliente', '=', $id)->update($datosCliente);

        $clientes = Clientes::findOrFail($id);

        return  redirect()->route('inicio', compact('clientes'))->with('success', ' El cliente fue actualizado');
    }


    //Este es metodo que me elimina los datos, requeriendo el id de la tabla de clientes, elimina todos los datos relacionados a ese id.
    public function destroy($id)
    {
        if (Auth::check()) {
            $clientes = Clientes::find($id);
            $clientes->delete();
            return redirect()->route('inicio')->with('success', ' El cliente fue elimindo');
        } else return Alert::error('Error Title', 'Error Message');
    }
    //Este es el metodo que me ejecuta la busqueda de departamentos mediante un select el cual esta relacionado a un pais por medio de un id.
    public function getDepartamentos($id)
    {
        $departamentos["departamento"] = departamentos::where('id_Paises', $id)->get();
        return Response()->json($departamentos, 200);
    }


    //
    /*public function getDepartamentos(Request $request)
    {
        if ($request->ajax()) {
            $departamentos = departamentos::where('id_Paises', $request->id_Paises)->get();
            foreach ($departamentos as $departamento) {
                $departamentosArray[$departamento->id_Departamento] = $departamento->nombre_departemento;
            }
            return response()->json($departamentosArray);
        }
    }*/
}
