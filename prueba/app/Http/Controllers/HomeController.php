<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clientes = Clientes::orderBy('idCliente')->get();
        $clientes->each(function ($clientes) {
            $clientes->paises;
            $clientes->departaments;

            $clientes->sexos;
        });
        return view('Cliente.index', compact('clientes'));
       
    }
}
