<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Clientes;
use App\Pais;

class departamentos extends Model
{
    //En el modelo se hace referecia a las relaciones y artributos de la tabla en la base de datos

    protected $table = 'departamentos';

    protected $primaryKey = 'id_Departamento';

    protected $fillable = ['nombre_departemento']; 

   public function client(){
       return $this->hasMany(Clientes::class);
   }
   
 
}
