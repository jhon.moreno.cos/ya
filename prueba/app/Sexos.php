<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Clientes;

class Sexos extends Model

{
    //En el modelo se hace referecia a las relaciones y artributos de la tabla en la base de datos

    protected $table = 'sexos';

    protected $primaryKey = 'idSexo';

    protected $fillable = ['tipoSexo'];

    public function clientes(){
        return $this->hasMany(Clientes::class);
    }

}
