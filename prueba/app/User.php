<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    //En el modelo se hace referecia a las relaciones y artributos de la tabla en la base de datos

    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

 
    protected $hidden = [
        'password', 'remember_token',
    ];

   
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   
}
