<?php 

namespace App\Services;

use App\Pais;

class Paises
{

    public function get()
    {
        $paises = Pais::get();
        $paisesarray[''] = 'Selecciona un pais';
        foreach($paises as $pais){
            $paisesarray[$pais->idPaises]=$pais->nombrePais;

        }
        return $paisesarray;
    }

}