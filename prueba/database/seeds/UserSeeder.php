<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{




    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 2; $i++) {

            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' =>  \Hash::make('123456789'),
            ]);
        }
    }
}
