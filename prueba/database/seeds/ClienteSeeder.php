<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;


class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $faker = Faker::create();
            DB::table('clientes')->insert([
                'nombreCliente' => $faker->firstName,
                'apellidoCliente' => $faker->lastName,
                'telefonoCliente' => $faker->numberBetween($min = 000000001, $max = 999999999),
                'id_Paises' => App\Pais::all()->random()->idPaises,
                'id_Sexo' => App\Sexos::all()->random()->idSexo,
                'id_Departamento' => App\departamentos::all()->random()->id_Departamento,

            ]);
        }
    }
}
