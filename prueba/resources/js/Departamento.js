$(document).ready(function () {

    $('.Pais').on('change', function () {
        var id_Paises = $('.Pais').val();
        $('#departamento').val(0);

        if ($.trim(id_Paises) != '') {
            $.get('departamentos', { id_Paises: id_Paises }, function (departamentos) {

                var old = $('#departamento').data('old') != '' ? $('#departamento').data('old') : '';

                $('#departamento').empty();
                $('#departamento').append("<option value=''>Selecciona un departamento</option>");

                $.each(departamentos, function (index, value) {
                    $('#departamento').append("<option value='" + index + "'" + (old == index ? 'selected' : '') + ">" + value + "</option>");
                })
            });
        }
    });
});

