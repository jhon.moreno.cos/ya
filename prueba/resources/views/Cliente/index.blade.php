@extends('layouts.app')

@section('content')
@inject('paises', 'App\Services\Paises')
@if (Route::has('login'))
<div class="container">

    @auth
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card ">
                <div class="card-header bg-info text-light text-center"> Clientes Regisrados</div>
                @if(session('flash'))
                <br>
                <div class="alert alert-success" role="alert">
                    <strong>Aviso:</strong>{{session('flash')}}
                    <button type="button" class="close" data-dismiss="alert" alert-lable="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if(session('success_message'))
                <br>
                <div class="alert alert-success">
                    {{ session('success_message')}}

                </div>
                @endif
                @if(session('error_message'))
                <br>
                <div class="alert alert-success">
                    {{ session('success_message')}}

                </div>
                @endif

                <div class="card-body">
                    <div class="table-responsive">
                        <div class="float-center text-center">
                            <a href="{{ route('registrar') }}" class=" text-light">
                                <i style="font-size:30px; color:#17a2b8;" class="fa">&#xf234;<h6>Añadir nuevo cliente</h6> </i>
                            </a>

                        </div>


                        <table class="table table-striped table-bordered" id="table_id" class="display">
                            <thead class="thead-light">
                                <th class="table-info">ID</th>
                                <th class="table-info">Nombre </th>
                                <th class="table-info">Apellido </th>
                                <th class="table-info">Sexo </th>
                                <th class="table-info">Teléfono </th>

                                <th class="table-info">Pais</th>
                                <th class="table-info">Departamento</th>
                                <th class="table-info">Acciones </th>

                            </thead>
                            <tbody>
                                @foreach ($clientes ?? '' as $cliente)
                                <tr class="table-active">
                                    <td>{{$cliente->idCliente}}</td>
                                    <td>{{$cliente->nombreCliente}}</td>
                                    <td>{{$cliente->apellidoCliente}}</td>
                                    <td>{{$cliente->sexos->tipoSexo}}</td>
                                    <td>{{$cliente->telefonoCliente}}</td>

                                    <td>{{$cliente->paises->nombrePais}}</td>
                                    <td>{{$cliente->departaments->nombre_departemento}}</td>

                                    <td>
                                   @auth
                                        <a href="{{ route('cliente.destroy', $cliente->idCliente)}}" onclick="return confirm('Seguro que quieres eliminarlo?')"><i style="font-size:35px; color:red;" class="fa">&#xf1f8;</i></a>

                                        <a href="{{ route('clientes.edit', $cliente->idCliente)}}"> <i style="font-size:35px; color:#17a2b8;" class="fa">&#xf14b; </i></a>
                                    @endauth
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>


                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>



</div>
@else
<h3 class="text-center">No tines acceso a esta pagina</h3>
@endauth

@endif
@endsection