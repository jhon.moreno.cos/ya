@extends('layouts.app')

@section('content')
@inject('paises', 'App\Services\Paises')

@inject('sexos', 'App\Services\sexs')

<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info text-white">Editar información</div>

                <div class="card-body">


                    <h1 class="text-center "><i style='font-size:50px' class='far'>&#xf044;</i></h1>
                    <form action="{{ route('clientes.update', $clientes)}}" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{ csrf_field()}}
                                {{ method_field('PATCH')}}
                                <label for="nombreCliente">Nombre</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <input type="text" value="{{ $clientes->nombreCliente}}" class="form-control {{ $errors->has('nombreCliente') ? ' is-invalid' : '' }}" value="{{ old('nombreCliente') }}" id="nombreCliente" name="nombreCliente">


                                    @error('nombreCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidoCliente">Apellido</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <input type="text" value="{{ $clientes->apellidoCliente}}" class="form-control {{ $errors->has('apellidoCliente') ? ' is-invalid' : '' }}" value="{{ old('apellidoCliente') }}" id="apellidoCliente" name="apellidoCliente">

                                    @error('apellidoCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">

                                <label for="sexoCliente">Sexo</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>

                                    <select name="id_Sexo" id="id_Sexo" class="form-control {{ $errors->has('id_Sexo') ? ' is-invalid' : '' }}" value="{{ old('apellidoCliente') }}">


                                        <option value="0">Seleccione una opción</option>

                                        @foreach ($sexss as $sexo)

                                        <option value="{{ $sexo->idSexo }}" {{ $sexo->idSexo == $idSexo ? 'selected' : '' }}>
                                            {{$sexo->tipoSexo}}
                                        </option>
                                        @endforeach

                                    </select>
                                    @error('id_Sexo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-group col-md-6">
                                <label for="telefonoCliente">Teléfono</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <input type="number" id="telefonoCliente" value="{{ $clientes->telefonoCliente}}" name="telefonoCliente" class="form-control {{ $errors->has('telefonoCliente') ? ' is-invalid' : '' }}" value="{{ old('telefonoCliente') }}">

                                    @error('telefonoCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Pais">País donde reside</label>
                                <label for="telefonoCliente">Teléfono</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style="font-size:18px" class="fa">&#xf124;</i></span>
                                    </div>
                                    <select name="id_Paises" id="Pais" class="Pais form-control {{ $errors->has('id_Paises') ? ' is-invalid' : '' }}">
                                        <option value="0">Seleccione una opción</option>
                                        @foreach ($paisess as $index => $pais)
                                        <option value="{{ $pais->idPaises }}" {{ $pais->idPaises == $idPaises ? 'selected' : '' }}>
                                            {{$pais->nombrePais}}
                                        </option>
                                        @endforeach

                                    </select>

                                    @if ($errors->has('id_Paises'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_Paises') }}</strong>
                                    </span>
                                    @endif
                                </div>


                            </div>
                            <div class="form-group col-md-6">
                                <label for="departamento">Departamento</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style="font-size:18px" class="fa">&#xf124;</i></span>
                                    </div>
                                    <select id="departamento" name="id_Departamento" class="form-control {{ $errors->has('id_Departamento') ? ' is-invalid' : '' }}">
                                        <option value="0">Seleccione una opción</option>
                                        @foreach ($departamentoos as $index => $departamento)
                                        <option value="{{ $departamento->id_Departamento }}" {{ $departamento->id_Departamento == $idDepartamento ? 'selected' : '' }}>
                                            {{$departamento->nombre_departemento}}
                                        </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('id_Departamento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_Departamento') }}</strong>
                                    </span>
                                    @endif
                                </div>


                            </div>
                        </div>



                        <button type="submit" class="btn btn-info text-white btn-lg btn-block">Actualizar</button>
                    </form>


                </div>
            </div>
        </div>
    </div>

    @else

    <h3 class="text-center">No tines acceso a esta pagina</h3>
    @endauth
</div>
@endsection