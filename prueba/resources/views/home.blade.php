@extends('layouts.app')

@section('content')
@inject('paises', 'App\Services\Paises')

@inject('sexos', 'App\Services\sexs')


<div class="container">
    @if(Auth::user())
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card  border-info ">
                <div class="card-header bg-info text-white  border-info ">Registrar cliente</div>

                <div class="card-body  ">


                    <h1 class="text-center ">Registrate</h1>
                    <form action="{{ route('cliente.store')}}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                {{ csrf_field()}}
                                <label for="nombreCliente">Nombre</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <input type="text" class="form-control {{ $errors->has('nombreCliente') ? ' is-invalid' : '' }}" value="{{ old('nombreCliente') }}" id="nombreCliente" name="nombreCliente">

                                    @error('nombreCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                            </div>
                            <div class="form-group col-md-6">

                                <label for="apellidoCliente">Apellido</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <input type="text" class="form-control {{ $errors->has('apellidoCliente') ? ' is-invalid' : '' }}" value="{{ old('apellidoCliente') }}" id="apellidoCliente" name="apellidoCliente" aria-describedby="basic-addon1">
                                    @error('apellidoCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>



                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="sexoCliente">Sexo</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <select name="id_Sexo" id="id_Sexo" class="form-control {{ $errors->has('id_Sexo') ? ' is-invalid' : '' }}">
                                        @foreach ($sexos ->get() as $index => $sexo)

                                        <option value="{{ $index }}" {{ old('id_Sexo') == $index ? 'selected' : '' }}>
                                            {{$sexo}}
                                        </option>
                                        @endforeach

                                    </select>
                                    @error('id_Sexo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>




                            </div>
                            <div class="form-group col-md-6">
                                <label for="telefonoCliente">Teléfono</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style='font-size:18px' class='fas'>&#xf406;</i></span>
                                    </div>
                                    <input type="number" class="form-control {{ $errors->has('telefonoCliente') ? ' is-invalid' : '' }}" id="telefonoCliente" name="telefonoCliente" value="{{ old('telefonoCliente') }}">

                                    @error('telefonoCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Pais">País donde reside</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style="font-size:18px" class="fa">&#xf124;</i></span>
                                    </div>
                                    <select name="id_Paises" id="Pais" class="Pais form-control {{ $errors->has('id_Paises') ? ' is-invalid' : '' }}" value="{{ old('id_Paises') }}">

                                        @foreach ($paises ->get() as $index => $pais)

                                        <option value="{{ $index }}" {{ old('id_Paises') == $index ? 'selected' : '' }}>
                                            {{$pais}}
                                        </option>
                                        @endforeach

                                    </select>
                                    @if ($errors->has('id_Paises'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_Paises') }}</strong>
                                    </span>
                                    @endif
                                </div>



                            </div>
                            <div class="form-group col-md-6">
                                <label for="departamento">Departamento</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text " id="basic-addon1"><i style="font-size:18px" class="fa">&#xf124;</i></span>
                                    </div>
                                    <select id="departamento" data-old="{{ old('id_Departamento') }}" name="id_Departamento" class="form-control {{ $errors->has('id_Departamento') ? ' is-invalid' : '' }}" value="{{ old('id_Departamento') }}">
                                        <option value="0">Seleccione una opción</option>

                                    </select>
                                    @if ($errors->has('id_Departamento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_Departamento') }}</strong>
                                    </span>
                                    @endif
                                </div>



                            </div>
                        </div>


                        <button type="submit" class="btn btn-info text-white btn-lg btn-block">Registrar</button>
                    </form>


                </div>
            </div>
        </div>
    </div>
    @endif
</div>

@endsection