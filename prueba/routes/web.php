<?php



Route::get('/', function () {
    
    return view('auth.login');
});

//LLama todas las funciones del controlador
Route::resource('clientes', 'ClientesController')->middleware('auth');

Auth::routes();

//llama la vista principal

Route::get('/Cliente/index', 'HomeController@index')->name('inicio');

//llama la vista y el metodo de guardar
Route::post('/clientes', 'ClientesController@store')->name('cliente.store')->middleware('auth');

//Llama las vistas del controlador
Route::get('/cliente', 'ClientesController@index');

//llama la vista y el metodo de actualizar
Route::post('/clientes/{id}/update', 'ClientesController@update')->name('cliente.update')->middleware('auth');

//llama los datos para el select anidado
Route::get('/departamentos/{id}', 'ClientesController@getDepartamentos');

//llama la vista y el metodo de eliminar

Route::get('clientes/{id}/destroy', [
    'uses'  => 'ClientesController@destroy',
    'as' => 'cliente.destroy',

])->middleware('auth');

//es una ruta para llamar la vista de la tabla 
Route::get('/home', [
    'uses'  => 'HomeController@index',
    'as' => 'cliente.index',
]);

//es una ruta para llamar la vista para registrar un nuevo cliente 


Route::get('/Registrar', [
    'uses'  => 'ClientesController@index',
    'as' => 'registrar',

])->middleware('auth');
